<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ユーザー管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="header">
		<form action="Signup">
			<input type="submit" value="ユーザー新規登録" />
		</form>
	</div>
	<div class="list-contents">
		<table>
			<tbody>
				<tr>
					<th>ID</th>
					<th>ログインID</th>
					<th>ユーザー名称</th>
					<th>支店名</th>
					<th>部署・役職</th>
					<th>作成日時</th>
					<th>更新日時</th>
					<th>編集</th>

				</tr>

				<c:forEach var="list" items="${user_data}">
					<tr>
						<td><c:out value="${list.getId()}" /></td>
						<td><c:out value="${list.getLogin_Id() }" /></td>
						<td><c:out value="${list.getName()}" /></td>
						<td><c:out value="${list.getBranch_name()}" /></td>
						<td><c:out value="${list.getPosition_name()}" /></td>
						<td><fmt:formatDate value="${list.getCreatedDate()}"
								pattern="yyyy/MM/dd HH:mm:ss" /></td>
						<td><fmt:formatDate value="${list.getUpdatedDate()}"
								pattern="yyyy/MM/dd HH:mm:ss" /></td>

						<td><span class="edit_button">
								<form action="Edit" method="get">
									<input type="hidden" name="userId" value="${list.getId()}">
									<input type="submit" value="編集" />
								</form>
						</span> <c:if test="${list.getStatus() == 0}">
								<span class="stop_button">
									<form action="AccountSetting" method="post">
										<input type="hidden" name="userId" value="${list.getId()}">
										<input type="hidden" name="accountStatus"
											value="${list.getStatus()}"> <input type="submit"
											value="停止" onClick="return confirm('アカウントを停止させますか？')" />
									</form>

								</span>
							</c:if>
							<c:if test="${list.getStatus() == 1}">
								<span class="active_button">

									<form action="AccountSetting" method="post">
										<input type="hidden" name="userId" value="${list.getId()}">
										<input type="hidden" name="accountStatus"
											value="${list.getStatus()}"> <input type="submit"
											value="復活" onClick="return confirm('アカウントを復活させますか？')" />
									</form>
								</span>
							</c:if></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>