<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ユーザー新規登録</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
	登録するユーザー情報を入力してください

	<div class="list-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach var="message" items="${errorMessages}">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="Signup" method="post">
			<br /> ■ログイン情報<br />
			<c:forEach items="${registUser}" var="userList">
				<label for="login_id">ログインID</label>

				<c:choose>
					<c:when test="${empty userList.getLogin_Id()}">
						<input name="login_id" id="login_id" placeholder="半角英数字で6～20文字" />
					</c:when>
					<c:otherwise>
						<input name="login_id" id="login_id"
							value="${userList.getLogin_Id()}" />
					</c:otherwise>
				</c:choose>
				<br />

				<label for="password">新規パスワード</label>
				<input name="password" type="password" id="password"
					placeholder="半角文字で6～20文字" />
				<br />
				<label for="password_sub">確認用パスワード</label>
				<input name="password_sub" type="password" id="password_sub"
					placeholder="半角文字で6～20文字" />
				<br />

				<br /> ■ユーザー情報<br />
				<label for="name">名前</label>
				<c:choose>
					<c:when test="${empty userList.getName()}">
						<input name="name" id="name" placeholder="10文字以下で登録可能" />
					</c:when>
					<c:otherwise>
						<input name="name" id="name" value="${userList.getName()}" />
					</c:otherwise>
				</c:choose>
				<br />

				<label for="branch">支店</label>
				<select name="branch">
					<c:forEach items="${branchsList}" var="branchList">
						<c:choose>
							<c:when test="${userList.getBranch() == branchList.getBranch()}">
								<option value=${branchList.getBranch() } selected>${branchList.getBranch_name()}</option>
							</c:when>
							<c:otherwise>
								<option value=${branchList.getBranch()} >${branchList.getBranch_name()}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>

				<br />
				<label for="position">部署・役職</label>
				<select name="position">
					<c:forEach items="${potisionsList}" var="potisionList">
						<c:choose>
							<c:when
								test="${userList.getPosition() == potisionList.getPosition()}">
								<option value=${potisionList.getPosition() } selected>${potisionList.getPosition_name()}</option>
							</c:when>
							<c:otherwise>
								<option value=${potisionList.getPosition()} >${potisionList.getPosition_name()}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</c:forEach>
			<br /> <input type="submit" value="登録" />

		</form>

		<form action="Manage">
			<input type="submit" value="キャンセル" />
		</form>
	</div>


</body>
</html>