<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ユーザー編集</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/main.css" rel="stylesheet" type="text/css">
</head>
<body>

	<div class="list-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach var="message" items="${errorMessages}">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="Edit" method="post">
			<table>
				<tbody>
					<tr>
						<th>ID</th>
						<th>ログインID</th>
						<th>ユーザー名称</th>
						<th>支店名</th>
						<th>部署・役職</th>
						<th>新規パスワード</th>
						<th>確認用パスワード</th>
					</tr>

					<c:forEach items="${editUser}" var="userList">
						<input type="hidden" name="userId" value="${userList.getId()}">

						<tr>
							<td><c:out value="${userList.getId()}" /></td>
							<td><input style="border: none;"
								background:none; name="login_id" id="login_id"
								value="${userList.getLogin_Id()}" /></td>
							<td><input style="border: none;"
								background:none; name="name" id="name"
								value="${userList.getName()}" /></td>

							<td><input style="border: none;"background:none;> <select
								name="branch">
									<c:forEach items="${branchsList}" var="branchList">
										<c:choose>
											<c:when
												test="${userList.getBranch() == branchList.getBranch()}">
												<option value=${branchList.getBranch() } selected>${branchList.getBranch_name()}</option>
											</c:when>
											<c:otherwise>
												<option value=${branchList.getBranch()} >${branchList.getBranch_name()}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
							</select></td>

							<td><input style="border: none;" background:none; /> <select
								name="position">
									<c:forEach items="${potisionsList}" var="potisionList">
										<c:choose>
											<c:when
												test="${userList.getPosition() == potisionList.getPosition()}">
												<option value=${potisionList.getPosition() } selected>${potisionList.getPosition_name()}</option>
											</c:when>
											<c:otherwise>
												<option value=${potisionList.getPosition()} >${potisionList.getPosition_name()}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>

							</select></td>

							<td><input type="password" style="border: none;"
								background:none;
								name="password" id="password"
								placeholder="半角文字で6～20文字" /></td>
							<td><input type="password" style="border: none;"
								background:none;
								name="password_sub" id="password_sub"
								placeholder="半角文字で6～20文字" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="submit" value="登録" />
		</form>
		<form action="Manage">
			<input type="submit" value="キャンセル" />
		</form>
	</div>
</body>
</html>