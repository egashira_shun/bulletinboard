package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.SQLRuntimeException;

public class UserListDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", user_name");
            sql.append(", branchs_id");
            sql.append(", position_id");
            sql.append(", created_at");
            sql.append(", updated_at");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch
            sql.append(", ?"); // position
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_Id());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getPosition());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<User> getUserList(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id, ");
            sql.append("users.login_id, ");
            sql.append("users.user_name, ");
            sql.append("branchs.branch_name, ");
            sql.append("positions.position_name, ");
            sql.append("users.status, ");
            sql.append("users.created_at, ");
            sql.append("users.updated_at ");
            sql.append("FROM users ");
            sql.append("LEFT JOIN branchs ");
            sql.append("ON users.branchs_id = branchs.branch_id ");
            sql.append("LEFT JOIN positions ");
            sql.append("ON users.position_id = positions.position_id ");
            sql.append("ORDER BY id ASC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<User> ret = toUserList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("users.id");
                String login_id = rs.getString("users.login_id");
                String user_name = rs.getString("users.user_name");
                String branch_name = rs.getString("branchs.branch_name");
                String position_name = rs.getString("positions.position_name");
                int status = rs.getInt("users.status");
                Timestamp created_at = rs.getTimestamp("users.created_at");
                Timestamp updated_at = rs.getTimestamp("users.updated_at");

                User user = new User();
                user.setId(id);
                user.setLogin_Id(login_id);
                user.setName(user_name);
                user.setBranch_name(branch_name);
                user.setPosition_name(position_name);
                user.setStatus(status);
                user.setCreatedDate(created_at);
                user.setUpdatedDate(updated_at);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}