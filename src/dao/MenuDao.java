package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.User;
import exception.SQLRuntimeException;

/**
 * Servlet implementation class MenuDao
 */
@WebServlet("/MenuDao")
public class MenuDao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ArrayList<User> getBranchsList(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = ("SELECT branch_id, branch_name FROM branchs ORDER BY branch_id ASC");
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			String requestList = "branchs";
			ArrayList<User> branchsList = toListData(rs, requestList);

			return branchsList;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public ArrayList<User> getPotisionsList(Connection connection) {

		PreparedStatement ps = null;
		try {
			String sql = ("SELECT position_id, position_name FROM positions ORDER BY position_id ASC");
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			String requestList = "positions";
			ArrayList<User> potisionsList = toListData(rs, requestList);

			return potisionsList;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean checkExist(int userId, String login_id, Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT id FROM users WHERE login_id = ");
			sql.append("?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, login_id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				int checkId = rs.getInt("id");
				if (checkId != userId && checkId != 0) {
					return true;
				}
			}
			return false;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private ArrayList<User> toListData(ResultSet rs, String requestList) throws SQLException {

		ArrayList<User> listData = new ArrayList<>();
		try {
			while (rs.next()) {
				User user = new User();

				switch (requestList) {
				case "branchs":
					user.setBranch(rs.getInt("branchs.branch_id"));
					user.setBranch_name(rs.getString("branchs.branch_name"));
					listData.add(user);
					break;

				case "positions":
					user.setPosition(rs.getInt("positions.position_id"));
					user.setPosition_name(rs.getString("positions.position_name"));
					listData.add(user);
					break;

				}
			}
			return listData;
		} finally {
			close(rs);
		}
	}

}
