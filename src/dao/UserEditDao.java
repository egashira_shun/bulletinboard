package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.User;
import exception.SQLRuntimeException;

/**
 * Servlet implementation class UserEditDao
 */
@WebServlet("/UserEditDao")
public class UserEditDao extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public List<User> getData(Connection connection, int userId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id, ");
			sql.append("users.login_id, ");
			sql.append("users.user_name, ");
			sql.append("users.branchs_id, ");
			sql.append("branchs.branch_name, ");
			sql.append("users.position_id, ");
			sql.append("positions.position_name, ");
			sql.append("users.status, ");
			sql.append("users.created_at, ");
			sql.append("users.updated_at ");
			sql.append("FROM users ");
			sql.append("LEFT JOIN branchs ");
			sql.append("ON users.branchs_id = branchs.branch_id ");
			sql.append("LEFT JOIN positions ");
			sql.append("ON users.position_id = positions.position_id ");
			sql.append("WHERE id = " + userId);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<User> userData = toUserData(rs);

			return userData;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserData(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {

				int id = rs.getInt("users.id");
				String login_id = rs.getString("users.login_id");
				String user_name = rs.getString("users.user_name");
				int branchs_id = rs.getInt("users.branchs_id");
				String branch_name = rs.getString("branchs.branch_name");
				int position_id = rs.getInt("users.position_id");
				String position_name = rs.getString("positions.position_name");
				int status = rs.getInt("users.status");
				Timestamp created_at = rs.getTimestamp("users.created_at");
				Timestamp updated_at = rs.getTimestamp("users.updated_at");

				User user = new User();
				user.setId(id);
				user.setLogin_Id(login_id);
				user.setName(user_name);
				user.setBranch(branchs_id);
				user.setBranch_name(branch_name);
				user.setPosition(position_id);
				user.setPosition_name(position_name);
				user.setStatus(status);
				user.setCreatedDate(created_at);
				user.setUpdatedDate(updated_at);
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public void dataEdit(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", user_name = ?");
			sql.append(", branchs_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_Id());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getPosition());
			ps.setInt(5, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void dataPassEdit(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", user_name = ?");
			sql.append(", branchs_id = ?");
			sql.append(", position_id = ?");
			sql.append(", password = ?");
			sql.append(", updated_at = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_Id());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getPosition());
			ps.setString(5, user.getPassword());
			ps.setInt(6, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void statusEdit(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" status =");
			sql.append(" ?");
			sql.append(", updated_at =");
			sql.append(" CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id =");
			sql.append(" ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getStatus());
			ps.setInt(2, user.getId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
