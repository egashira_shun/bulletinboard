package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.User;
import dao.UserEditDao;
import utils.CipherUtil;

/**
 * Servlet implementation class EditService
 */
@WebServlet("/EditService")
public class EditService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public List<User> getUserData(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserEditDao UserDataDao = new UserEditDao();
			List<User> userData = UserDataDao.getData(connection, userId);

			commit(connection);

			return userData;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void dataEditer(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserEditDao editDao = new UserEditDao();
            editDao.dataEdit(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void dataPassEditer(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserEditDao editDao = new UserEditDao();
            editDao.dataPassEdit(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void statusEditer(User user) {
		Connection connection = null;
        try {
            connection = getConnection();

            UserEditDao editDao = new UserEditDao();
            editDao.statusEdit(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
