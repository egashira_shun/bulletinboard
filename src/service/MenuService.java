package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.User;
import dao.MenuDao;

/**
 * Servlet implementation class MenuService
 */
@WebServlet("/MenuService")
public class MenuService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ArrayList<User> getBranchsList() {

		Connection connection = null;
		try {
			connection = getConnection();

			MenuDao getDataDao = new MenuDao();
			ArrayList<User> branchsList = getDataDao.getBranchsList(connection);

			commit(connection);

			return branchsList;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public ArrayList<User> getPotisionsList() {

		Connection connection = null;
		try {
			connection = getConnection();

			MenuDao getDataDao = new MenuDao();
			ArrayList<User> potisionsList = getDataDao.getPotisionsList(connection);

			commit(connection);

			return potisionsList;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	public boolean isExist(int userId, String login_id){
		Connection connection = null;
		try {
			connection = getConnection();

			MenuDao listDao = new MenuDao();
			boolean ret = listDao.checkExist(userId, login_id, connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
