package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.EditService;

/**
 * Servlet implementation class AccountServlet
 */
@WebServlet("/AccountSetting")
public class AccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		int userId = Integer.parseInt(request.getParameter("userId"));
		int accountStatus = Integer.parseInt(request.getParameter("accountStatus"));
		if(accountStatus == 0) {
			accountStatus = 1;
		}
		else if(accountStatus == 1) {
			accountStatus = 0;
		}

		User user = new User();
		user.setId(userId);
		user.setStatus(accountStatus);
		new EditService().statusEditer(user);
		response.sendRedirect("Manage");
	}
}