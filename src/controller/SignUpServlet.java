package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.MenuService;
import service.UserService;

@WebServlet("/Signup")
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<User> userData = new ArrayList<User>();
		User user = new User();
		user.setLogin_Id(null);
		user.setName(null);
		user.setBranch(0);
		user.setPosition(0);
		userData.add(user);
		request.setAttribute("registUser", userData);

		MenuService brList = new MenuService();
		ArrayList<User> branchsList = brList.getBranchsList();
		request.setAttribute("branchsList", branchsList);

		MenuService poList = new MenuService();
		ArrayList<User> potisionsList = poList.getPotisionsList();
		request.setAttribute("potisionsList", potisionsList);

		request.getRequestDispatcher("/signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		User user = new User();
		user.setLogin_Id(request.getParameter("login_id"));
		user.setName(request.getParameter("name"));
		user.setBranch(Integer.parseInt(request.getParameter("branch")));
		user.setPosition(Integer.parseInt(request.getParameter("position")));

		if (isValid(request, messages) == true) {
			user.setPassword(request.getParameter("password"));
			new UserService().register(user);
			response.sendRedirect("Manage");

		} else {
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", messages);

			List<User> userData = new ArrayList<User>();
			userData.add(user);
			request.setAttribute("registUser", userData);

			MenuService brList = new MenuService();
			ArrayList<User> branchsList = brList.getBranchsList();
			request.setAttribute("branchsList", branchsList);

			MenuService poList = new MenuService();
			ArrayList<User> potisionsList = poList.getPotisionsList();
			request.setAttribute("potisionsList", potisionsList);

			request.getRequestDispatcher("/signup.jsp").forward(request, response);

		}
	}

	//入力情報のチェック
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		int userId = 0;
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password_sub = request.getParameter("password_sub");
		String name = request.getParameter("name");

		//ログインIDのチェック
		if (StringUtils.isBlank(login_id)) {
			messages.add("ログインIDを入力してください");
		} else if (!login_id.matches("[a-zA-Z0-9]{6,20}")) {
			messages.add("ログインIDは半角英数字の6～20文字で使用可能です");
		} else if (new MenuService().isExist(userId, login_id)) {
			messages.add("既に使用されているログインIDです");
		}

		//ユーザー名称のチェック
		if (StringUtils.isBlank(name)) {
			messages.add("ユーザー名称を入力してください");
		} else if (name.matches(".{11,}")) {
			messages.add("ユーザー名称は10文字以下で入力してください");
		}

		//パスワードのチェック
		if (StringUtils.isBlank(password)) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("[a-zA-Z0-9 -/:-@\\[-\\`\\{-\\~]{6,20}")) {
			messages.add("パスワードは半角英数字の6～20文字で使用可能です");
		} else if (!(password.equals(password_sub))) {
			messages.add("新規パスワードと確認用パスワードが一致しません");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}